<?php

namespace Drupal\lb_flipcard\Plugin\Layout;

use Drupal\Core\Layout\LayoutDefault;

/**
 * A very advanced custom layout.
 *
 * @Layout(
 *   id = "lb_flipcard",
 *   label = @Translation("Flipcard"),
 *   category = @Translation("Effects"),
 *   path = "layout",
 *   template = "flipcard",
 *   library = "lb_flipcard/flipcard",
 *   regions = {
 *     "front" = {
 *       "label" = @Translation("Front"),
 *     },
 *     "back" = {
 *       "label" = @Translation("Back"),
 *     },
 *   }
 * )
 */
class FlipcardLayout extends LayoutDefault {}
